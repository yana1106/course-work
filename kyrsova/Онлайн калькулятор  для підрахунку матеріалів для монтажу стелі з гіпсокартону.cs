﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender,  EventArgs e)
        {
       double k;
       double s;   
       double kl;  
            s =Convert.ToDouble(label21.Text); //кількість листів
            if (s <= 10) 
                k = 1.3;
            if (s <= 20)
                k = 1.2;
            else
                k = 1.0;
            kl = Math.Ceiling((s / 3)*k);
            label6.Text = kl.ToString();


            double Kc;  // кількість сд профілів
            Double L, H;
            L = Convert.ToDouble(textBox4.Text);
            H = Convert.ToDouble(textBox5.Text);
            Kc = Math.Ceiling((kl * 3) - 1);
            label8.Text = Kc.ToString();

            double ud; // кількість ud профілів 
            ud = Math.Ceiling((Convert.ToDouble(label22.Text)) / 3);
            label10.Text = ud.ToString();

            double pks; // кількість кріплень сд профілів
            pks = Math.Ceiling((L * (H / 0.4 - 1)) / 0.4);
            label12.Text = pks.ToString();


            double kds; // кількість дюбелів з саморізами
            double obr1;
            L = Convert.ToDouble(textBox4.Text);
            H = Convert.ToDouble(textBox5.Text);
            obr1 = (L + H) * 2;
            kds = Math.Ceiling((obr1 / 0.5) + pks * 2);
            label14.Text = kds.ToString();


            double kch; // кількість чорних саморізів
            kch = Math.Ceiling((ud + Kc) * 12 + (pks * 2));
            label16.Text = kch.ToString();

            double shs;
            shs = Math.Ceiling(1.8 * 2 * (Convert.ToDouble(label21.Text)));
            label18.Text = shs.ToString();

            double shf;
            shf = Math.Ceiling(1.1 * 2 * (Convert.ToDouble(label21.Text)));
            label20.Text = shf.ToString();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Double L , H;
            double obr;
            L = Convert.ToDouble(textBox4.Text);
            H = Convert.ToDouble(textBox5.Text);
            obr = L * H;
            label21.Text = obr.ToString("n");
            
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Double L , H;
            double obr1;
            L = Convert.ToDouble(textBox4.Text);
            H = Convert.ToDouble(textBox5.Text);
            obr1 = (L + H) * 2;
            label22.Text = obr1.ToString("n");
        }

        

        private void textBox4_KeyPress(object sender, KeyPressEventArgs e)
        {
            char number = e.KeyChar;
            if ((e.KeyChar <= 47 || e.KeyChar >= 58) && number != 8 && (e.KeyChar <= 39 || e.KeyChar >= 46) && number != 47 && number != 61) //калькулятор
            {
                e.Handled = true;
            }
        }
        
         private void textBox5_KeyPress(object sender, KeyPressEventArgs e)
        {
            char number = e.KeyChar;
            if ((e.KeyChar <= 47 || e.KeyChar >= 58) && number != 8 && (e.KeyChar <= 39 || e.KeyChar >= 46) && number != 47 && number != 61) //калькулятор
            {
                e.Handled = true;
            }
            }

        

        private void textBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            char number = e.KeyChar;
            if ((e.KeyChar <= 47 || e.KeyChar >= 58) && number != 8 && (e.KeyChar <= 39 || e.KeyChar >= 46) && number != 47 && number != 61) //калькулятор
            {
                e.Handled = true;
            }
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            char number = e.KeyChar;
            if ((e.KeyChar <= 47 || e.KeyChar >= 58) && number != 8 && number != 44) //цифры, клавиша BackSpace и запятая а ASCII
            {
                e.Handled = true;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            label6.Text = "";
            label8.Text = "";
            label10.Text = "";
            label12.Text = "";
            label14.Text = "";
            label16.Text = "";
            label18.Text = "";
            label20.Text = "";
            label21.Text = "";
            label22.Text = "";
            textBox4.Text = "";
            textBox5.Text = "";
        }

      
        

      
         }
        }
    
    

